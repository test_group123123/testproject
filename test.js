// ==UserScript==
// @name         FirstScript
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  Log 'hello world' into console.
// @author       You
// @match        https://danieldusek.com/*
// @require      test.js
// @grant        none
// ==/UserScript==
console.log('Hello world');

if (typeof logExternal === 'function') { // Function defined in utils.js
    console.log('Hello from external file.');
} else { console.log('Unable to import utils.js'); }​